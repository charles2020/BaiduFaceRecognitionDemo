package com.baidu.aip;

import com.baidu.idl.facesdk.FaceInfo;

/**
 * @author :      fangbingran
 * @aescription : 自定义人脸识别回调
 * @date :        2019/10/19  19:44
 */
public interface CustomFaceFrameCallback {
    void showFrame(FaceInfo info, ImageFrame frame);
}
