package com.baidu.aip;

import android.graphics.Bitmap;


/**
 * @author :      fangbingran
 * @aescription : 人脸识别成功回调
 * @date :        2019/10/19  19:44
 */
public interface FaceRecognitionCallback {
    void onFace(Bitmap info);
    void onPress(String tip);
}
