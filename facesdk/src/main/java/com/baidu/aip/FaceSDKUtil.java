package com.baidu.aip;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.RectF;
import android.util.Log;
import android.view.TextureView;

import androidx.core.app.ActivityCompat;

import com.baidu.aip.face.CameraImageSource;
import com.baidu.aip.face.DetectRegionProcessor;
import com.baidu.aip.face.FaceCropper;
import com.baidu.aip.face.FaceDetectManager;
import com.baidu.aip.face.PreviewView;
import com.baidu.aip.face.camera.ICameraControl;
import com.baidu.aip.face.camera.PermissionCallback;
import com.baidu.aip.widget.BrightnessTools;
import com.baidu.idl.facesdk.FaceInfo;
import com.baidu.idl.facesdk.FaceSDK;
import com.baidu.idl.facesdk.FaceTracker;

import java.lang.ref.WeakReference;

import demo.face.aip.baidu.com.facesdk.R;

/**
 * @author :      fangbingran
 * @aescription : FaceSDK工具
 * @date :        2019/10/19  18:24
 */
public class FaceSDKUtil {
    /**
     * 初始化
     *
     * @param context
     * @param licenseId
     * @param licenseFileName
     */
    public static void init(Context context, String licenseId, String licenseFileName) {
        // 为了android和ios 区分授权，appId=appname_face_android ,其中appname为申请sdk时的应用名
        // 应用上下文
        // 申请License取得的APPID
        // assets目录下License文件名
        FaceSDKManager.getInstance().init(context, licenseId, licenseFileName);
        setFaceConfig(context);

    }

    /**
     * 初始化SDK
     */
    private void initLib() {
        // 为了android和ios 区分授权，appId=appname_face_android ,其中appname为申请sdk时的应用名
        // 应用上下文
        // 申请License取得的APPID
        // assets目录下License文件名

    }


    private static void setFaceConfig(Context context) {
        FaceTracker tracker = FaceSDKManager.getInstance().getFaceTracker(context.getApplicationContext());
        // SDK初始化已经设置完默认参数（推荐参数），您也根据实际需求进行数值调整

        // 模糊度范围 (0-1) 推荐小于0.7
        tracker.set_blur_thr(FaceEnvironment.VALUE_BLURNESS);
        // 光照范围 (0-1) 推荐大于40
        tracker.set_illum_thr(FaceEnvironment.VALUE_BRIGHTNESS);
        // 裁剪人脸大小
        tracker.set_cropFaceSize(FaceEnvironment.VALUE_CROP_FACE_SIZE);
        // 人脸yaw,pitch,row 角度，范围（-45，45），推荐-15-15
        tracker.set_eulur_angle_thr(FaceEnvironment.VALUE_HEAD_PITCH, FaceEnvironment.VALUE_HEAD_ROLL,
                FaceEnvironment.VALUE_HEAD_YAW);

        // 最小检测人脸（在图片人脸能够被检测到最小值）80-200， 越小越耗性能，推荐120-200
        tracker.set_min_face_size(FaceEnvironment.VALUE_MIN_FACE_SIZE);
        //
        tracker.set_notFace_thr(FaceEnvironment.VALUE_NOT_FACE_THRESHOLD);
        // 人脸遮挡范围 （0-1） 推荐小于0.5
        tracker.set_occlu_thr(FaceEnvironment.VALUE_OCCLUSION);
        // 是否进行质量检测
        tracker.set_isCheckQuality(true);
        // 是否进行活体校验
        tracker.set_isVerifyLive(false);
    }

    // textureView用于绘制人脸框等。
    private static TextureView mTextureView;
    private static Paint paint = new Paint();
    private static FaceTracker mTracker;
    private static boolean mIsPortrait = true;
    private static FaceDetectManager faceDetectManager;
    private static DetectRegionProcessor cropProcessor = new DetectRegionProcessor();
    private static PreviewView mPreviewView;
    private static int mRound = 2;
    private static int mScreenW;
    private static int mScreenH;
    private static boolean mDetectStoped = false;
    private static boolean mDetectBitmap = false;

    public static void initFaceDetect(Activity activity, PreviewView previewView, TextureView textureView, int round, int screenW, int screenH) {
        faceDetectManager = new FaceDetectManager(activity);
        mPreviewView = previewView;
        mTextureView = textureView;
        mRound = round;
        mScreenW = screenW;
        mScreenH = screenH;
        faceDetectManager.setDetectMax(1);
        textureView.setOpaque(false);
        // 不需要屏幕自动变黑。
        textureView.setKeepScreenOn(true);
        initCamera(activity);
        initBrightness(activity);
        initPaint();
        initTracker(activity);
    }

    /**
     * 初始化画笔
     */
    private static void initPaint() {
        paint.setColor(Color.BLUE);
        paint.setStyle(Paint.Style.STROKE);
    }


    /**
     * 人脸跟踪参数设置，参数可调整按自己需求设置
     */
    private static void initTracker(Activity activity) {
        mTracker = FaceSDKManager.getInstance().getFaceTracker(activity);
        mTracker.set_isFineAlign(false);
        mTracker.set_isVerifyLive(false);
        mTracker.set_DetectMethodType(1);
        mTracker.set_isCheckQuality(false);
        mTracker.set_notFace_thr(0.6f);
        mTracker.set_min_face_size(200);
        mTracker.set_cropFaceSize(400);
        mTracker.set_illum_thr(40);
        mTracker.set_blur_thr(0.7f);
        mTracker.set_occlu_thr(0.5f);
        mTracker.set_max_reg_img_num(1);
        mTracker.set_eulur_angle_thr(10, 10, 10);

        mTracker.set_track_by_detection_interval(80);
        FaceSDK.setNumberOfThreads(4);

    }

    /**
     * 初始化屏幕亮度，不到200自动调整到200
     */
    private static void initBrightness(Activity context) {
        int brightness = BrightnessTools.getScreenBrightness(context);
        if (brightness < 200) {
            BrightnessTools.setBrightness(context, 200);
        }
    }

    private static int mCurFaceId = -1;
    private static final double ANGLE = 8;

    /**
     * 初始化相机
     */
    private static void initCamera(final Activity context) {
        // 初始化相机图片资源
        final CameraImageSource cameraImageSource = new CameraImageSource(context);
        // 设置预览界面
        cameraImageSource.setPreviewView(mPreviewView);

        // 设置人脸检测图片资源
        faceDetectManager.setImageSource(cameraImageSource);
        // 设置人脸检测回调,其中 retCode为人脸检测回调值（0通常为检测到人脸),infos为人脸信息，frame为相机回调图片资源
        faceDetectManager.setOnFaceDetectListener(new FaceDetectManager.OnFaceDetectListener() {
            @Override
            public void onDetectFace(final int retCode, final FaceInfo[] infos, ImageFrame frame) {
                final FaceInfo faceInfo = (infos != null && infos[0] != null) ? infos[0] : null;
                if (mCustomFaceFrameCallback != null) {
                    mCustomFaceFrameCallback.showFrame(faceInfo, frame);
                    return;
                }
                String str = "";
                boolean mGoodDetect = false;
                if (retCode == 0) {
                    boolean distance = false;
                    if (faceInfo != null && frame != null) {
                        if (faceInfo.mWidth >= (0.9 * frame.getWidth())) {
                            distance = false;
                            str = context.getResources().getString(R.string.detect_zoom_out);
                        } else if (faceInfo.mWidth <= 0.2 * frame.getWidth()) {
                            distance = false;
                            str = context.getResources().getString(R.string.detect_zoom_in);
                        } else {
                            distance = true;
                        }
                    }
                    boolean headUpDown;
                    if (faceInfo != null) {
                        if (faceInfo.headPose[0] >= ANGLE) {
                            headUpDown = false;
                            str = context.getResources().getString(R.string.detect_head_up);
                        } else if (faceInfo.headPose[0] <= -ANGLE) {
                            headUpDown = false;
                            str = context.getResources().getString(R.string.detect_head_down);
                        } else {
                            headUpDown = true;
                        }

                        boolean headLeftRight;
                        if (faceInfo.headPose[1] >= ANGLE) {
                            headLeftRight = false;
                            str = context.getResources().getString(R.string.detect_head_left);
                        } else if (faceInfo.headPose[1] <= -ANGLE) {
                            headLeftRight = false;
                            str = context.getResources().getString(R.string.detect_head_right);
                        } else {
                            headLeftRight = true;
                        }

                        if (distance && headUpDown && headLeftRight) {
                            mGoodDetect = true;
                            str = "";
                        } else {
                            mGoodDetect = false;
                        }

                    }
                } else {
                    if (retCode == 1) {
                        str = context.getResources().getString(R.string.detect_head_up);
                    } else if (retCode == 2) {
                        str = context.getResources().getString(R.string.detect_head_down);
                    } else if (retCode == 3) {
                        str = context.getResources().getString(R.string.detect_head_left);
                    } else if (retCode == 4) {
                        str = context.getResources().getString(R.string.detect_head_right);
                    } else if (retCode == 5) {
                        str = context.getResources().getString(R.string.detect_low_light);
                    } else if (retCode == 6) {
                        str = context.getResources().getString(R.string.detect_face_in);
                    } else if (retCode == 7) {
                        str = context.getResources().getString(R.string.detect_face_in);
                    } else if (retCode == 10) {
                        str = context.getResources().getString(R.string.detect_keep);
                    } else if (retCode == 11) {
                        str = context.getResources().getString(R.string.detect_occ_right_eye);
                    } else if (retCode == 12) {
                        str = context.getResources().getString(R.string.detect_occ_left_eye);
                    } else if (retCode == 13) {
                        str = context.getResources().getString(R.string.detect_occ_nose);
                    } else if (retCode == 14) {
                        str = context.getResources().getString(R.string.detect_occ_mouth);
                    } else if (retCode == 15) {
                        str = context.getResources().getString(R.string.detect_right_contour);
                    } else if (retCode == 16) {
                        str = context.getResources().getString(R.string.detect_left_contour);
                    } else if (retCode == 17) {
                        str = context.getResources().getString(R.string.detect_chin_contour);
                    }
                    if (faceInfo != null) {
                        mGoodDetect = true;
                        str = "";
                    }
                }
                context.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
//                        不需要追踪人脸
                        showFrame(faceInfo);
                    }
                });
//                boolean faceChanged = false;
//                if (faceInfo != null) {
//                    Log.d("DetectLogin", "face id is:" + faceInfo.face_id);
//                    if (faceInfo.face_id == mCurFaceId) {
//                        faceChanged = false;
//                    } else {
//                        faceChanged = true;
//                    }
//                    mCurFaceId = faceInfo.face_id;
//                }

                if (mGoodDetect) {
                    if (mFaceRecognitionCallback != null) {
                        mFaceRecognitionCallback.onPress(str);
                        if (mDetectBitmap) {
                            return;
                        }
                        mDetectBitmap = true;
                        Bitmap face = FaceCropper.getFace(frame.getArgb(), faceInfo, frame.getWidth());
                        mWeakReference = new WeakReference<Bitmap>(face);
                        mFaceRecognitionCallback.onFace(mWeakReference.get());
                        mCurFaceId = -1;
                    }
                } else {
                    mFaceRecognitionCallback.onPress(str);
                }


            }
        });

        cameraImageSource.getCameraControl().setPermissionCallback(new PermissionCallback() {
            @Override
            public boolean onRequestPermission() {
                ActivityCompat.requestPermissions(context,
                        new String[]{Manifest.permission.CAMERA}, 100);
                return true;
            }
        });


        ICameraControl control = cameraImageSource.getCameraControl();
        control.setPreviewView(mPreviewView);
        // 设置检测裁剪处理器
        faceDetectManager.addPreProcessor(cropProcessor);

        // 获取相机屏幕方向
        int orientation = context.getResources().getConfiguration().orientation;
        mIsPortrait = (orientation == Configuration.ORIENTATION_PORTRAIT);

        // 根据屏幕方向决定预览拉伸类型
        if (mIsPortrait) {
            mPreviewView.setScaleType(PreviewView.ScaleType.FIT_WIDTH);
        } else {
            mPreviewView.setScaleType(PreviewView.ScaleType.FIT_HEIGHT);
        }

        int rotation = context.getWindowManager().getDefaultDisplay().getRotation();
        cameraImageSource.getCameraControl().setDisplayOrientation(rotation);
        // 设置相机摄像头类型，包括前置、后置及usb等类型
        setCameraType(cameraImageSource);
    }

    public static void setDetectBitmap(boolean detectBitmap) {
        mDetectBitmap = detectBitmap;
    }

    private static WeakReference<Bitmap> mWeakReference;

    /**
     * 摄像头类型设置，可根据自己需求设置前置、后置及usb摄像头
     *
     * @param cameraImageSource
     */
    private static void setCameraType(CameraImageSource cameraImageSource) {
        // TODO 选择使用前置摄像头
//        cameraImageSource.getCameraControl().setCameraFacing(ICameraControl.CAMERA_FACING_FRONT);

        // TODO 选择使用usb摄像头
        cameraImageSource.getCameraControl().setCameraFacing(ICameraControl.CAMERA_USB);
        // 如果不设置，人脸框会镜像，显示不准
        mPreviewView.getTextureView().setScaleX(-1);

        // TODO 选择使用后置摄像头
        // cameraImageSource.getCameraControl().setCameraFacing(ICameraControl.CAMERA_FACING_BACK);
        // previewView.getTextureView().setScaleX(-1);
    }


    /**
     * 绘制人脸框。
     *
     * @param info 追踪到的人脸
     */
    private static void showFrame(FaceInfo info) {
        if (mTextureView == null) {
            return;
        }
        Canvas canvas = mTextureView.lockCanvas();
        if (canvas == null) {
            return;
        }

        // 清空canvas
        canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
        if (info != null) {

            Log.d("liujinhui", " has frame");
            RectF rectCenter = new RectF(info.mCenter_x - 2 - info.mWidth * 3 / 5,
                    info.mCenter_y - 2 - info.mWidth * 3 / 5,
                    info.mCenter_x + 2 + info.mWidth * 3 / 5,
                    info.mCenter_y + 2 + info.mWidth * 3 / 5);
            mPreviewView.mapFromOriginalRect(rectCenter);
            // 绘制框
            paint.setStrokeWidth(mRound);
            paint.setAntiAlias(true);
            canvas.drawRect(rectCenter, paint);

//            // 画脸部各个点
//            if (info.landmarks.length > 0) {
//                int len = info.landmarks.length;
//                for (int i = 0; i < len; i += 2) {
//                    RectF rectPoint = new RectF(info.landmarks[i] - mRound, info.landmarks[i + 1] - mRound,
//                            info.landmarks[i] + mRound, info.landmarks[i + 1] + mRound);
//                    mPreviewView.mapFromOriginalRect(rectPoint);
//
//                    paint.setStrokeWidth(rectPoint.width() * 2 / 3);
//                    canvas.drawCircle(rectPoint.centerX(), rectPoint.centerY(), rectPoint.width() / 2, paint);
//
//                }
//            }
        } else {
            canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
        }
        mTextureView.unlockCanvasAndPost(canvas);
    }

    public static void resume() {
        if (mDetectStoped) {
            mCurFaceId = -1;
            faceDetectManager.start();
            mDetectStoped = false;

        }

    }

    /**
     * 人脸检测启动
     */
    public static void start() {
        RectF newDetectedRect = new RectF(0, 0, mScreenW, mScreenH);
        cropProcessor.setDetectedRect(newDetectedRect);
        faceDetectManager.start();
    }

    public static void stop() {
        if (faceDetectManager != null) {
            faceDetectManager.stop();
        }
        mDetectStoped = true;
        mCurFaceId = -1;
    }

    public static void destroy() {
        if (faceDetectManager != null) {
            faceDetectManager.setOnFaceDetectListener(null);
        }
        stop();
        faceDetectManager = null;
        mDetectStoped = false;
        mTextureView = null;
        mPreviewView = null;
        mTracker = null;
        mCustomFaceFrameCallback = null;
        mFaceRecognitionCallback = null;
        mRound = 2;
        mIsPortrait = true;
    }

    private static CustomFaceFrameCallback mCustomFaceFrameCallback;

    /**
     * 自定义人脸回调
     *
     * @param customFaceFrameCallback
     */
    public static void setCustomFaceFrameCallback(CustomFaceFrameCallback customFaceFrameCallback) {
        mCustomFaceFrameCallback = customFaceFrameCallback;
    }

    private static FaceRecognitionCallback mFaceRecognitionCallback;

    /**
     * 人脸识别结果回调
     *
     * @param faceRecognitionCallback
     */
    public static void setFaceRecognitionCallback(FaceRecognitionCallback faceRecognitionCallback) {
        mFaceRecognitionCallback = faceRecognitionCallback;
    }

}
