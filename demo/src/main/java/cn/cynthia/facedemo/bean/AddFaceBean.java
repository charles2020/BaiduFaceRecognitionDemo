package cn.cynthia.facedemo.bean;

/**
 * Created by guoshuifang on 2019/12/3.
 * TODO
 */
public class AddFaceBean {
    /*log_id	是	uint64	请求标识码，随机数，唯一
    face_token	是	string	人脸图片的唯一标识
    location	是	array	人脸在图片中的位置
+left	是	double	人脸区域离左边界的距离
+top	是	double	人脸区域离上边界的距离
+width	是	double	人脸区域的宽度
+height	是	double	人脸区域的高度
+rotation	是	int64	人脸框相对于竖直方向的顺时针旋转角，[-180,180]
*/
    public String face_token;
    public LocationBean location;
    public String error_code;//
    public String error_msg;//
    public static class LocationBean {

        public int left;
        public int top;
        public int width;
        public int height;
        public int rotation;
    }
}
