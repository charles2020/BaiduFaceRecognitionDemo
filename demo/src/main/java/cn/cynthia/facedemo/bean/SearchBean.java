package cn.cynthia.facedemo.bean;

import java.util.List;

/**
 * Created by guoshuifang on 2019/12/3.
 * TODO
 */
public class SearchBean {

    public int error_code;
    public String error_msg;
    public long log_id;
    public int timestamp;
    public int cached;
    public ResultBean result;

    public static class ResultBean {

        public String face_token;
        public List<UserListBean> user_list;//匹配的用户信息列表

        public static class UserListBean {
            public String group_id;//用户所属的group_id
            public String user_id;//用户的user_id
            public String user_info;//注册用户时携带的user_info
            public double score;//用户的匹配得分，推荐阈值80分
        }
    }
}
