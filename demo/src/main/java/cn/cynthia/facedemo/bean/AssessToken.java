package cn.cynthia.facedemo.bean;

/**
 * Created by guoshuifang on 2019/12/3.
 * TODO
 */
public class AssessToken {
    public String refresh_token;
    public long expires_in;//Access Token的有效期(秒为单位，一般为1个月)；
    public String scope;
    public String session_key;
    public String access_token;// 要获取的Access Token；
    public String session_secret;
    public String error;//invalid_client
    public String error_description;//unknown client id  = API Key 不正确 , Client authentication failed = Secret Key不正确

}
