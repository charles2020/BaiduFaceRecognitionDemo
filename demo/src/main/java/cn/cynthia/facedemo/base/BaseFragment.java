package cn.cynthia.facedemo.base;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;

import cn.dlc.commonlibrary.ui.base.BaseCommonFragment;
import cn.dlc.commonlibrary.utils.BindEventBus;
import me.yokeyword.fragmentation.anim.DefaultNoAnimator;
import me.yokeyword.fragmentation.anim.FragmentAnimator;

public abstract class BaseFragment extends BaseCommonFragment {
    @Override
    public FragmentAnimator onCreateFragmentAnimator() {
        // 取消动画
        return new DefaultNoAnimator();
    }

    private boolean isResume;//是否resume
    private boolean isHidden;//是否隐藏
    private boolean isCreate;//是否已经创建

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        isCreate = true;
    }

    @Override
    public void onResume() {
        super.onResume();
        isResume = true;
    }

    @Override
    public void onPause() {
        super.onPause();
        isResume = false;
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        isHidden = hidden;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        isCreate = false;
    }

    //是否正在展示
    protected boolean isShowing() {
        return isResume&& !isHidden && isCreate;
    }

    @Nullable
    @Override
    public View onCreateView(
        LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

}
