package cn.cynthia.facedemo;

import com.google.gson.reflect.TypeToken;
import com.lzy.okgo.model.HttpParams;

import java.util.UUID;

import cn.cynthia.facedemo.base.http.MyStringConvert;
import cn.cynthia.facedemo.bean.AddFaceBean;
import cn.cynthia.facedemo.bean.AssessToken;
import cn.cynthia.facedemo.bean.SearchBean;
import cn.cynthia.facedemo.utils.SpUtil;
import cn.dlc.commonlibrary.okgo.OkGoWrapper;
import io.reactivex.Observable;

/**
 * Created by guoshuifang on 2020/1/4.
 * TODO
 */
public class HttpManager {
    private final OkGoWrapper mOkGoWrapper;
    //获取百度的assess token
    public Observable<AssessToken> getAssessToken() {
        HttpParams params = new HttpParams();
        params.put("grant_type", "client_credentials");
        params.put("client_id", Information.BAIDU_API_KEY);
        params.put("client_secret", Information.BAIDU_SECRET_KEY);
        return mOkGoWrapper.rxPost(Urls.getAssessToken, params, new MyStringConvert<AssessToken>(new TypeToken<AssessToken>() {
        }.getType()));
    }

    //添加人脸
    public Observable<AddFaceBean> addFace(String image, String image_type) {
        HttpParams params = new HttpParams();
        params.put("access_token", SpUtil.getAssessToken() != null ? SpUtil.getAssessToken().access_token : "");
        /*图片信息(总数据大小应小于10M)，图片上传方式根据image_type来判断。
        注：组内每个uid下的人脸图片数目上限为20张*/
        params.put("image", image);
/*        图片类型
        BASE64:图片的base64值，base64编码后的图片数据，编码后的图片大小不超过2M；
        URL:图片的 URL地址( 可能由于网络等原因导致下载图片时间过长)；
        FACE_TOKEN：人脸图片的唯一标识，调用人脸检测接口时，会为每个人脸图片赋予一个唯一的FACE_TOKEN，
        同一张图片多次检测得到的FACE_TOKEN是同一个。*/
        params.put("image_type", image_type);
        params.put("group_id", "1");//组id
        params.put("user_id", UUID.randomUUID().toString().replace("-","_"));//用户id
        return mOkGoWrapper.rxPost(Urls.addFace, params, new MyStringConvert<AddFaceBean>(new TypeToken<AddFaceBean>() {
        }.getType()));
    }

    //人脸搜索
    public Observable<SearchBean> searchFace(String image, String image_type) {
        HttpParams params = new HttpParams();
        params.put("access_token", SpUtil.getAssessToken() != null ? SpUtil.getAssessToken().access_token : "");
        /*图片信息(总数据大小应小于10M)，图片上传方式根据image_type来判断。
        注：组内每个uid下的人脸图片数目上限为20张*/
        params.put("image", image);
/*        图片类型
        BASE64:图片的base64值，base64编码后的图片数据，编码后的图片大小不超过2M；
        URL:图片的 URL地址( 可能由于网络等原因导致下载图片时间过长)；
        FACE_TOKEN：人脸图片的唯一标识，调用人脸检测接口时，会为每个人脸图片赋予一个唯一的FACE_TOKEN，
        同一张图片多次检测得到的FACE_TOKEN是同一个。*/
        params.put("image_type", image_type);
        params.put("group_id_list", "1");//从指定的group中进行查找 用逗号分隔，上限10个
        return mOkGoWrapper.rxPost(Urls.searchFace, params, new MyStringConvert<SearchBean>(new TypeToken<SearchBean>() {
        }.getType()));
    }


    static class HttpManagerHolder {
        static HttpManager instance = new HttpManager();
    }

    private HttpManager() {
        mOkGoWrapper = OkGoWrapper.instance();
    }

    public static HttpManager getInstance() {
        return HttpManagerHolder.instance;
    }
}
