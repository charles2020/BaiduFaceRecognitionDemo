package cn.cynthia.facedemo.utils;

import com.google.gson.Gson;

import cn.cynthia.facedemo.bean.AssessToken;
import cn.dlc.commonlibrary.utils.PrefUtil;

/**
 * Created by guoshuifang on 2020/1/4.
 * TODO
 */
public class SpUtil {
    public static AssessToken getAssessToken() {
        return new Gson().fromJson(PrefUtil.getDefault().getString("assessToken", ""), AssessToken.class);
    }

    public static void saveAssessToken(AssessToken assessToken) {
        PrefUtil.getDefault().saveString("assessToken", new Gson().toJson(assessToken));
    }

}
